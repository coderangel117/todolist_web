const pool = require("../db/config");
const {extractBearerToken} = require("../utils/bearerToken");
const jwt = require("jsonwebtoken");
const date = require("date-and-time");


module.exports = (app) => {

    app.get("/tasks", (req, res) => {
        pool.query("SELECT * FROM tasks", (err, result) => {
            if (err) res.status(400).json({
                state: "failure", message: err,
            }); else {
                res.json({result})
            }
        })
    })
    app.get("/task/list/:id", (req, res) => {
        const id = parseInt(req.params.id);

        pool.query("SELECT * FROM tasks WHERE list_id = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    state: "failure", message: err
                });
            } else {
                res.json({result})
            }
        })
    })
    app.get("/task/:id", (req, res) => {
        const id = parseInt(req.params.id);

        pool.query("SELECT * FROM tasks WHERE id = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    state: "failure",
                    message: err,
                });
            } else {
                res.status(200).json({
                    "task": result,
                })
            }
        });
    });
    app.post("/task/create", (req, res) => {
        let libelle = req.body.libelle;
        let list_id = req.body.list_id;
        if (libelle && list_id) {
            pool.query(
                "INSERT INTO tasks (libelle, list_id) values (?, ?)", [libelle, list_id], (err, result) => {
                    if (err) {
                        res.status(400).json({
                            state: "failure", message: {
                                code: err.code,
                                message: err.sqlMessage
                            },
                        });
                    } else {
                        res.status(201).json({
                            state: "success", message: {
                                task: result
                            },
                        });
                    }
                });
        } else {
            return res.status(400).json({
                state: "failure",
                message: "Missing parameters",
            });
        }
    });
    app.put("/task/:id", (req, res) => {
        const id = parseInt(req.params.id);
        let libelle = req.body.libelle;
        let list_id = req.body.list_id;
        if (libelle && list_id) {
            pool.query("UPDATE tasks SET libelle = ? , list_id = ? WHERE id = ?",
                [libelle, list_id, id],
                (err, result) => {
                    if (err) {
                        res.status(400).json({
                            state: "failure",
                            message: err,
                        });
                    } else if (result.affectedRows > 0) {
                        res.status(200).json({
                            state: "success",
                            task: result
                        });
                    } else res.status(404).json({
                        state: "failure", message: "task not found or nothing to update", result: result,
                    });
                });
        } else {
            return res.status(400).json({
                state: "failure",
                message: "Missing parameters",
            });
        }
    });
    app.delete("/task/:id", (req, res) => {
        const id = parseInt(req.params.id);
        pool.query("DELETE FROM tasks WHERE id = ?", [id], (err, result) => {
            if (err) res.status(400).json({
                state: "failure", message: err,
            }); else if (result.affectedRows > 0) res.json({
                state: "success", message: "task deleted", result: result,
            }); else res.status(404).json({
                state: "failure", message: "task not found",
            });
        });
    });
}
