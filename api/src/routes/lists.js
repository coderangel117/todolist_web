const pool = require("../db/config");
const {extractBearerToken} = require("../utils/bearerToken");
const jwt = require("jsonwebtoken");
const date = require("date-and-time");


module.exports = (app) => {
    app.get("/lists", (req, res) => {
        pool.query("SELECT * FROM lists", (err, result) => {
            if (err) res.status(400).json({
                state: "failure", message: err,
            }); else {
                res.json({result})
            }
        })
    })

    app.get("/list/user/:id", (req, res) => {
        const id = parseInt(req.params.id);
        pool.query("SELECT * FROM lists WHERE owner = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    state: "failure",
                    message: err.sqlMessage,
                });
            } else {
                res.status(200).json({
                    state: "success",
                    message: result,
                })
            }
        })
    })

    app.get("/list/:id", (req, res) => {
        const id = parseInt(req.params.id);

        pool.query("SELECT * FROM lists WHERE id = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    state: "failure",
                    message: err.sqlMessage,
                });
            } else {
                res.status(200).json({
                    state: "success",
                    message: result,
                })
            }
        });
    });

    app.post("/list/create", (req, res) => {
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        const decoded = jwt.decode(token, {complete: false});
        const libelle = req.body.libelle;
        const owner = decoded.id;
        const category_id = req.body.category_id;

        // Vérifie si l'utilisateur existe
        pool.query('SELECT * FROM users WHERE id = ?', [decoded.id], (err, userResult) => {
            if (err) {
                // Gérer l'erreur
                return res.status(500).json({
                    state: "failure",
                    message: "Internal server error",
                });
            }

            if (userResult.length === 0) {
                // L'utilisateur n'existe pas, renvoyer une erreur
                return res.status(400).json({
                    state: "failure",
                    message: "User not found",
                });
            }

            // Vérifie si la catégorie existe
            pool.query('SELECT * FROM categories WHERE id = ?', [category_id], (err, categoryResult) => {
                if (err) {
                    // Gérer l'erreur
                    return res.status(500).json({
                        state: "failure",
                        message: "Internal server error",
                    });
                }

                if (categoryResult.length === 0) {
                    // La catégorie n'existe pas, renvoyer une erreur
                    return res.status(400).json({
                        state: "failure",
                        message: "Category not found",
                    });
                }

                // L'utilisateur existe et la catégorie existe, vous pouvez insérer la liste
                pool.query(
                    "INSERT INTO lists (libelle, owner, category_id) VALUES (?, ?, ?)",
                    [libelle, owner, category_id],
                    (err, result) => {
                        if (err) {
                            res.status(400).json({
                                state: "failure",
                                message: err.sqlMessage,
                            });
                        } else {
                            res.status(201).json({
                                state: "success",
                                message: result,
                            });
                        }
                    }
                );
            });
        });
    });


    app.put("/list/:id", (req, res) => {
        const id =  req.params.id
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        const decoded = jwt.decode(token, {complete: false});
        const libelle = req.body.libelle;
        const owner = decoded.id;
        const category_id = req.body.category_id;
        pool.query("UPDATE lists SET libelle = ?, owner = ?, category_id= ?  WHERE id = ?",
            [libelle, owner, category_id, id], (err, result) => {
                if (err) {
                    return res.status(400).json({
                        state: "failure",
                        message: err,
                    });
                } else if (result.affectedRows > 0) {
                    return res.status(200).json({
                        state: "success",
                        list: result,
                    });
                } else res.status(404).json({
                    state: "failure", message: "list not found or nothing to update", result: result,
                });
            });
    });

    app.delete("/list/:id", (req, res) => {
        const id = parseInt(req.params.id);
        pool.query("DELETE FROM lists WHERE id = ?", [id], (err, result) => {
            if (err) res.status(400).json({
                state: "failure", message: err,
            }); else if (result.affectedRows > 0) res.json({
                state: "success", message: "list deleted", result: result,
            }); else res.status(404).json({
                state: "failure", message: "list not found",
            });
        });
    });

}
