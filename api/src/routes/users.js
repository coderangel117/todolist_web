const {checktoken} = require("../utils/bearerToken");
const pool = require("../db/config");
module.exports = (app) => {

    app.get("/users", checktoken, (req, res) => {
        pool.query("SELECT * FROM users", (err, result) => {
            if (err) res.status(400).json({
                state: "failure", message: err,
            }); else res.status(200).json(result);
        });
    });
    app.get("/user/:id", checktoken, (req, res) => {
        const id = parseInt(req.params.id);
        pool.query("SELECT * FROM users WHERE id = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    status: 400, state: "failure", message: err,
                });
            } else if (result.length > 0) {
                res.json({result: result});
            } else {
                res.status(404).json({
                    status: 404,
                    state: "failure",
                    message: "User not found",
                });
            }
        });
    });
    app.put("/user/:id", checktoken, (req, res) => {
        const id = parseInt(req.params.id);
        const {login} = req.body
        if (!login) {
            return res.status(400).json({
                state: "failure",
                message: "Missing parameters",
            });
        }
        pool.query(
            "UPDATE users SET login = ? WHERE id = ?",
            [login, id], (err, result) => {
                if (err) {
                    res.status(400).json({
                        state: "failure",
                        message: err,
                    });
                } else if (result.affectedRows > 0) {
                    res.json({
                        result: result
                    });
                } else {
                    res.status(404).json({
                        state: "failure",
                        message: "User not found or nothing updated",
                        result: result,
                    });
                }
            });
    });
    app.delete("/user/:id", checktoken, (req, res) => {
        const id = parseInt(req.params.id);
        if (!id) {
            console.log(req.params.id)
            res.status(400).json({
                state: "failure",
                message: "Missing parameters",
            });
        }
        pool.query('Select * from users where id = ?', [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    status: 400,
                    state: "failure",
                    message: err.sqlMessage,
                });
            } else if (result.affectedRows === 0) {
                res.json({
                    state: "failure",
                    message: "User not found",
                });
            }
        })
        pool.query("DELETE FROM users WHERE id = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    status: 400,
                    state: "failure",
                    message: err.sqlMessage,
                });
            } else if (result.affectedRows > 0) {
                res.json({result});
            } else {
                res.status(404).json({
                    state: "failure",
                    message: "User not found",
                });
            }
        });
    });
}
