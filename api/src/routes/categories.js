const pool = require("../db/config");
module.exports = (app) => {

    app.get("/categories", (req, res) => {
        pool.query("SELECT * FROM  categories", (err, result) => {
            if (err) res.status(400).json({
                state: "failure", message: err,
            }); else res.status(200).json(result);
        });
    });
    app.get("/category/:id", (req, res) => {
        const id = parseInt(req.params.id);
        pool.query("SELECT * FROM  categories WHERE id = ?", [id], (err, result) => {
            if (err) {
                res.status(400).json({
                    status: 400, state: "failure", message: err,
                });
            } else if (result.length > 0) {
                res.json({result: result});
            } else {
                res.status(404).json({
                    status: 404,
                    state: "failure",
                    message: " category not found",
                });
            }
        });
    });
    app.put("/category/:id", (req, res) => {
        const id = parseInt(req.params.id);
        const {libelle} = req.body
        if (!libelle) {
            res.status(400).json({
                state: "failure",
                message: "Missing parameters",
            });
        }
        pool.query(
            "UPDATE  categories SET  libelle = ? WHERE id = ?",
            [libelle, id], (err, result) => {
                if (err) {
                    res.status(400).json({
                        state: "failure",
                        message: err,
                    });
                } else if (result.affectedRows > 0) {
                    res.json({
                        result: result
                    });
                } else {
                    res.status(404).json({
                        state: "failure",
                        message: " category not found or nothing updated",
                        result: result,
                    });
                }
            });
    });

    app.post("/category/create", (req, res) => {
        let libelle = req.body.libelle
        if (libelle) {
            pool.query(
                "INSERT INTO categories (libelle) values (?)",
                [libelle], (err, result) => {
                    if (err) res.status(400).json({
                        state: "failure", message: {
                            code: err.code,
                            message: err.message
                        },
                    }); else res.status(201).json({result});
                });
        } else {
            return res.status(400).json({
                state: "failure", message: "Missing parameters",
            });
        }
    });

    app.delete("/category/:id", (req, res) => {
        const id = parseInt(req.params.id);
        if (!id) {
            return res.status(400).json({
                state: "failure",
                message: "Missing parameters",
            });
        }

        pool.query('SELECT * FROM categories WHERE id = ?', [id], (err, result) => {
            if (err) {
                return res.status(400).json({
                    status: 400,
                    state: "failure",
                    message: err,
                });
            } else if (result.length === 0) {
                return res.status(404).json({
                    state: "failure",
                    message: "Category not found",
                });
            }
            else if(result.length > 0) {
                pool.query("DELETE FROM categories WHERE id = ?", [id], (err, result) => {
                    if (err) {
                        return res.status(400).json({
                            status: 400,
                            state: "failure in sql query",
                            message: err,
                        });
                    } else if (result.affectedRows > 0) {
                        return res.status(200).json({
                            state: "success",
                            message: "Category deleted successfully",
                        });
                    }
                });
            }

        });
    });
}
