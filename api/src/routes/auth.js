const pool = require("../db/config");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {checktoken, extractBearerToken} = require("../utils/bearerToken");

module.exports = (app) => {
    app.get('/me', checktoken, (req, res) => {
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        const decoded = jwt.decode(token, {complete: false});
        return res.status(200).json({
            id: decoded.id,
            login: decoded.login,
            role: decoded.role,
        });
    });
    app.post('/login', (req, res) => {
        if (req.body.login && req.body.password) {
            let login = req.body.login;
            let password = req.body.password;
            pool.query("SELECT * FROM users WHERE login = ?", [login], (err, result) => {
                if (err) {
                    res.status(400).json({
                        message: err,
                    });
                } else if (result.affectedRows === 0) {
                    res.status(404).json({
                        state: "failure", message: "user not found", result: result,
                    });
                } else {
                    const isValidPass = bcrypt.compareSync(password, result[0].password);
                    if (isValidPass) {
                        const token = jwt.sign({
                            id: result[0].id,
                            login: result[0].login,
                            role: result[0].role,
                        }, 'secret', {expiresIn: "24 hours"});
                        res.header('Authorization', 'Bearer ' + token);
                        return res.json(token.toString());
                    } else {
                        res.status(401).json({
                            state: "failure", message: "Invalid credentials",
                        });
                    }
                }
            });
        } else {
            return res.status(400).json({
                state: "failure", message: "Missing parameters",
            });
        }
    })
    ;
    app.post("/register", (req, res) => {
        let login = req.body.username;
        let password = req.body.password;
        if (login && password) {
            const salt = bcrypt.genSaltSync(10)
            const hash = bcrypt.hashSync(password, salt)
            pool.query("INSERT INTO users (login, password, role) values (?, ?, ?)", [login, hash, 2], (err, result) => {
                if (err) {
                    res.status(400).json({
                        status: 400, message: err,
                    });
                } else {
                    res.status(201).json({
                        state: "success", message: "User registered", result: result,
                    });
                }
            });
        } else {
            return res.status(400).json({
                state: "failure", message: "Missing parameters",
            });
        }
    });
}
