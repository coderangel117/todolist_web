DROP DATABASE IF EXISTS `todolist_web`;
CREATE DATABASE IF NOT EXISTS `todolist_web` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `todolist_web`;
create table categories
(
    id      int auto_increment
        primary key,
    libelle text not null,
    constraint categories_id_uindex
        unique (id)
);

create table role
(
    idrole  int auto_increment
        primary key,
    libelle varchar(50) not null
);

create table users
(
    id       int auto_increment
        primary key,
    login    varchar(50)  null,
    password varchar(150) not null,
    role     int          null,
    constraint users_id_uindex
        unique (id),
    constraint users_role_idRole_fk
        foreign key (role) references role (idrole)
            on update cascade on delete cascade
);

create table lists
(
    id          int auto_increment
        primary key,
    libelle     text not null,
    owner       int  null,
    category_id int  not null,
    constraint tasks_id_uindex
        unique (id),
    constraint tasks_categories_id_fk
        foreign key (category_id) references categories (id)
            on update cascade on delete cascade,
    constraint tasks_users_id_fk
        foreign key (owner) references users (id)
            on update cascade on delete cascade
);

create table tasks
(
    id      int auto_increment
        primary key,
    libelle text not null,
    list_id int  not null,
    constraint tasks_id_uindex
        unique (id),
    constraint tasks_lists_id_fk
        foreign key (list_id) references lists (id)
            on update cascade on delete cascade
);

INSERT INTO `categories` (`id`, `libelle`)
VALUES (1, 'administratif'),
       (2, 'logiciel'),
       (3, 'cscs');


INSERT INTO `role` (`idrole`, `libelle`)
VALUES (1, 'administrateur'),
       (2, 'visiteur');

INSERT INTO `users` (`id`, `login`, `password`, `role`)
VALUES (1, 'test', '$2y$10$HFefddt/hHjIUpT1xlIHV.HaSquTdvsfoEOX2Rvhrj.YYe0J8WpuS', 2),
 (2, 'test', '$2y$10$HFefddt/hHjIUpT1xlIHV.HaSquTdvsfoEOX2Rvhrj.YYe0J8WpuS', 2);

INSERT INTO `lists` (`id`, `libelle`, `owner`, `category_id`)
VALUES (1, 'foo', 1, 2),
       (2, 'foo', 2, 2),
       (3, 'foo', 2, 2);

INSERT INTO `tasks` (`id`, `libelle`, `list_id`)
VALUES (1, 'foo', 2),
       (2, 'foo', 3),
       (3, 'foo', 2),
       (4, 'foo', 3);