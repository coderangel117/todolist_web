const mysql = require('mysql');
const dotenv = require('dotenv');
dotenv.config();


const config = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
};
// Create a MySQL pool
const pool = mysql.createPool(config);

// Export the pool
module.exports = pool;
