const jwt = require("jsonwebtoken");

const extractBearerToken = (headerValue) => {
    if (typeof headerValue !== "string") {
        return null;
    }
    const matches = headerValue.match(/(bearer)\s+(\S+)/i);
    matches[2] = matches[2].replace(/[ """]/gi, "")
    return matches && matches[2];
};


const checktoken = (req, res, next) => {
    const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
    if (!token) {
        return res.status(401).json({message: "Error No token"});
    }
    jwt.verify(token, "secret", (err, decodedToken) => {
        if (err) {
            return res.status(401).json({message: "Error Bad token "});
        } else {
            return next();
        }
    });
};
module.exports = {
    checktoken,
    extractBearerToken
}
