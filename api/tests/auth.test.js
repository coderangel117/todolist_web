const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
const expect = chai.expect;
const Id = 1;
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywibG9naW4iOiJ0ZXN0dXNlciIsInJvbGUiOjIsImlhdCI6MTY5NDk4MTI5OSwiZXhwIjoxNjk1MDY3Njk5fQ.5pxBR-KAY_lrl5Bvxs1OdaxG3j7_icR5gJfSOeb5unA'
describe('auth Tests', () => {
    it('register', (done) => {
        const userData = {
            username: 'testuser',
            password: 'testpassword',
        };
        chai.request(app)
            .post(`/register`)
            .send(userData)
            .end((err, res, req) => {
                expect(res).to.have.status(201);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('state', 'success');
                done();
            });
    });
    it('login', (done, req) => {
        const userData = {
            username: 'testuser',
            password: 'testpassword',
        };
        chai.request(app)
            .post(`/login`)
            .send(userData)
            .end((err, res, req) => {
                expect(res).to.have.status(200);
                done();
            });
    });

    it('me ', (done) => {
        chai.request(app)
            .get(`/me`)
            .set('Authorization', `Bearer ${token}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
});
