const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
const expect = chai.expect;
const Id = 1;

describe('categories Tests', () => {
    it('get all categories ', (done) => {
        chai.request(app)
            .get(`/categories`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('get category by id ', (done, req) => {
        chai.request(app)
            .get(`/category/${Id}`)

            .end((err, res, req) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it(' Create category ', (done) => {
        const categoryData = {
            libelle: 'administratif'
        };
        chai.request(app)
            .post(`/category/create`)
            .send(categoryData)
            .end((err, res) => {
                expect(res).to.have.status(201);
                done();
            });
    });
    it('Update category ', (done) => {
        const updatedCategoryData = {
            libelle: 'Updated Category Name',
        };
        chai.request(app)
            .put(`/category/${Id}`)
            .send(updatedCategoryData)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('Delete category ', (done) => {
        chai.request(app)
            .delete(`/category/${Id}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
});
