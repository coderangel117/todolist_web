const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
const expect = chai.expect;
const Id = 1;
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywibG9naW4iOiJ0ZXN0dXNlciIsInJvbGUiOjIsImlhdCI6MTY5NDk4MTI5OSwiZXhwIjoxNjk1MDY3Njk5fQ.5pxBR-KAY_lrl5Bvxs1OdaxG3j7_icR5gJfSOeb5unA'

describe('Users Tests', () => {
    it('get all users ', (done) => {
        chai.request(app)
            .get(`/users`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('get user by id ', (done, req) => {
        chai.request(app)
            .get(`/user/${Id}`)
            .set('Authorization', `Bearer ${token}`)
            .end((err, res, req) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('Update user ', (done) => {
        const userData = {
            login: 'testuser',
            password:'testpassword'
        };
        chai.request(app)
            .put(`/user/${Id}`)
            .send(userData)
            .set('Authorization', `Bearer ${token}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('Delete user ', (done) => {
        chai.request(app)
            .delete(`/user/${Id}`)
            .set('Authorization', `Bearer ${token}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done()
            });
    });
});
