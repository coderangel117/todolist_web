const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
const expect = chai.expect;
const Id = 1;
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywibG9naW4iOiJ0ZXN0dXNlciIsInJvbGUiOjIsImlhdCI6MTY5NDk4MTI5OSwiZXhwIjoxNjk1MDY3Njk5fQ.5pxBR-KAY_lrl5Bvxs1OdaxG3j7_icR5gJfSOeb5unA'

describe('Lists Tests', () => {
    it('get all lists ', (done) => {
        chai.request(app)
            .get(`/lists`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('get list by id ', (done, req) => {
        chai.request(app)
            .get(`/list/${Id}`)

            .end((err, res, req) => {
                expect(res).to.have.status(200);
                done();
            });
    });

    it('get lists by user id  ', (done) => {
        chai.request(app)
            .get(`/list/user/${Id}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it(' Create list ', (done) => {
        const listData = {
            libelle: 'foo',
            category_id: 2
        };
        chai.request(app)
            .post(`/list/create`)
            .set('Authorization', `Bearer ${token}`)
            .send(listData)
            .end((err, res) => {
                expect(res).to.have.status(201);
                done();
            });
    });
    it('Update list ', (done) => {
        const listData = {
            libelle: 'foo',
            category_id: 2
        };
        chai.request(app)
            .put(`/list/${Id}`)
            .set('Authorization', `Bearer ${token}`)
            .send(listData)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('Delete list ', (done) => {
        chai.request(app)
            .delete(`/list/${Id}`)
            .set('Authorization', `Bearer ${token}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
});
