const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
const expect = chai.expect;
const Id = 2;
const listId = 3;

describe('Tasks Tests', () => {
    it('get', (done) => {
        chai.request(app)
            .get(`/`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('get all tasks ', (done) => {
        chai.request(app)
            .get(`/tasks`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('get task by id ', (done, req) => {
        chai.request(app)
            .get(`/task/${Id}`)

            .end((err, res, req) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('get tasks by list id  ', (done) => {
        chai.request(app)
            .get(`/task/list/${listId}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
    it(' Create task ', (done) => {
        const taskData = {
            libelle: 'foo',
            list_id:3
        };
        chai.request(app)
            .post(`/task/create`)
            .send(taskData)
            .end((err, res) => {
                console.log(res.body)
                expect(res).to.have.status(201);
                done();
            });
    });
    it('Update task ', (done) => {
        const taskData = {
            libelle: 'foo',
            list_id:3
        };
        chai.request(app)
            .put(`/task/${Id}`)
            .send(taskData)
            .end((err, res) => {
                console.log(res.body)
                expect(res).to.have.status(200);
                done();
            });
    });
    it('Delete task ', (done) => {
        chai.request(app)
            .delete(`/task/${Id}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
});
