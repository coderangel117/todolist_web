const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require('body-parser');
const app = express();
const Port = 3000;

const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 204,// For legacy browser support
    optionsFailureStatus: 400,
    methods: "GET, PUT, POST, DELETE, HEAD, OPTIONS"
} // set options to fix cross origin errors


app.use(cors(corsOptions))
    .use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    }) // another cors options
    .use(express.urlencoded({extended: true})) // encode form data
    .use(bodyParser.urlencoded({ extended: true }))
    .use(express.json()) // set response data in json format
    .use(morgan("dev"))  // debug in console
    .listen(Port, () => console.log(`Server started on http://localhost:${Port}`)); //run server


app.get("/", (req, res) => {
    res.send({
        status: 200,
        message: "Welcome to the Node.js Express REST API!",
    });
});

/* require files with all routes*/

require('./src/routes/users')(app)
require('./src/routes/categories')(app)
require('./src/routes/lists')(app)
require('./src/routes/tasks')(app)
require('./src/routes/auth')(app)

module.exports = app;