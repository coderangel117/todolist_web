# Groupe de perino_g 994493
# API 

## Requis avant de pouvoir utliser l'API

Avoir node d'installé sur votre ordinateur  
Pour vérifier vous devez taper cette commande dans le cmd de votre ordinateur  

```node -v```   
 ```npm -v```  

Si vous avez une erreur suite a une de ses commandes, vous devez installer ce qui manque.
Dans ce cas, vous devez aller sur le site de nodejs et installer l'éxécutable en fonction de votre système d'exploitation  

 [page de téléchargement de nodejs](https://nodejs.org/en/download)


## Installation 

Pour récuperer le contenu du repository en effectuant la commande suivantee dans le terminal de votre ordinateur   

```git clone https://gitlab.com/coderangel117/todolist_web.git```

Ensuite placer vous dedans à l'aide de   


```cd todolist_web/API```

Pour récupérer les packages de nodejs, vous devez effectuer la commande suivante:  
    ``` npm install ```

Une fois ces étapes terminées vous pouvez démarrer l'api avec la commande  

```npm start```
