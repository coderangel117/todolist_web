import axios from 'axios';
import {AccountService} from "@/_services";
import router from "@/router";

const Axios = axios.create({
        baseURL: 'http://localhost:3000'
    }
)

Axios.interceptors.response.use(response => {
        return response
    }, error => {
        if (error.response.status === 401) {
            AccountService.logout()
            router.push("/login")
        }
    }
)

Axios.interceptors.request.use(request => {
        let token = AccountService.getToken()
        if (token) {
            request.headers.Authorization = 'Bearer ' + token
        }
        return request
    }
)
export default Axios