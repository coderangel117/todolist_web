import Axios from "@/_services/caller.service";


let login = (credentials) => {
    return Axios.post('login', credentials)
}
let logout = () => {
    let token = localStorage.key(0)
    localStorage.removeItem(token);
}
let getToken = () => {
    return localStorage.key(0)
}

let decodeToken = () => {
    let token = getToken()
    if(token){
        const [headerEncoded, payloadEncoded] = token.split(".");
        return JSON.parse(atob(payloadEncoded));
    }
    else{
        return false
    }
}
let saveToken = (token) => {
    localStorage.setItem(token, 'token')
}

let isLogged = () => {
    let token = localStorage.key(0)
    return !!token
}


export const AccountService = {
    login, logout, getToken, saveToken, isLogged, decodeToken
}
