import {createStore} from 'vuex';
import {AccountService, decodeToken} from "@/_services";

const store = createStore({
    state: {
        user: {
            id: 0,
            login: '',
            role: 0,
        },
        isLoggedIn: false,
    },
    mutations: {
        setIsLoggedIn(state, isLogged) {
            state.isLoggedIn = isLogged
        },
        setUser(state, user) {
            state.user = {
                id: user.id,
                login: user.login,
                role: user.role,
            };
        },
        logout(state) {
            state.isLoggedIn = false;
        },
    },
    actions: {
        loadUserFromLocalStorage({commit}) {
            let userData = AccountService.decodeToken()
            if(!userData){
                userData = {
                    id: 0,
                    login: '',
                    role: 0,
                }
                commit('setUser', userData);
            }
            commit('setUser', userData);
        },
        checkAuthentication({commit}) {
            const logged = AccountService.isLogged()
            if (logged) {
                commit('setIsLoggedIn', true);
            } else {
                commit('setIsLoggedIn', false);
            }
        },
        async login({commit}, credentials) {
            try {
                const res = await AccountService.login(credentials);
                AccountService.saveToken(res.data);
                let userData = AccountService.decodeToken()
                console.log(userData)
                commit('setUser', userData);
                // commit('setUser', credentials.login);
                commit('setIsLoggedIn', true);
            } catch (error) {
                console.error("from store " + error);
                throw error;
            }
        },

        async logout({commit}) {
            try {
                await AccountService.logout();
                commit('logout');
            } catch (error) {
                // Gérez les erreurs de déconnexion ici
                console.error(error);
                throw error; // Propagez l'erreur pour que le composant puisse la gérer
            }
        },
    },
    getters: {
        currentUser(state) {
            return state.user;
        },
        isAuthenticated(state) {
            return state.isLoggedIn;
        },
    },
});

export default store;
