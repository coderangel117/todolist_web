import router from "@/router";
import {AccountService} from "@/_services";

export function AuthGuard() {
    let token = AccountService.getToken()
    if (token) {
        let role = AccountService.decodeToken().role
        if(role === 2){
            return true
        }else{
            router.push('/')
        }
    } else {
        router.push('/login')
    }
}

export function LoginGuard() {
    if (AccountService.isLogged()) {
        router.push('/')
    }
}
