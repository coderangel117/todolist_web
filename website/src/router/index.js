import {createRouter, createWebHistory} from 'vue-router'
import Home from "@/views/public/Home.vue";
import Login from "@/views/auth/Login.vue";
import AdminLayout from "@/views/admin/AdminLayout.vue";
import Dashboard from "@/views/admin/Dashboard.vue";
import PublicLayout from "@/views/public/PublicLayout.vue";
import {AuthGuard, LoginGuard} from "@/_helpers/auth-guard";
import users from "@/views/public/Users.vue";

const routes = [
    {
        path: '/admin', name: 'admin', component: AdminLayout,
        children: [
            {path: '/dashboard', name: 'Dashboard', component: Dashboard},
            {path: '/:pathMatch(.*)*', redirect: '/'}
        ]
    },
    {
        path: '/', name: 'public', component: PublicLayout,
        children: [
            {path: '/', name: 'home', component: Home},
            {path: '/users', name: 'users', component: users},
            {path: '/login', name: 'login', component: Login, beforeEnter: LoginGuard},
            {path: '/:pathMatch(.*)*', redirect: '/'},
        ]
    },
    {path: '/:pathMatch(.*)*', redirect: '/'}]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL), routes
})


router.beforeEach(to => {
    if (to.matched[0].name === 'admin') {
        AuthGuard()
    }
})
export default router
